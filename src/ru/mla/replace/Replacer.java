package ru.mla.replace;

import ru.mla.replace.printers.IPrinter;
import ru.mla.replace.readers.IReader;

import java.io.IOException;

class Replacer {

    private IReader reader;
    private IPrinter printer;

    public Replacer(IReader reader, IPrinter printer) {
        this.reader = reader;
        this.printer = printer;
    }

    void replace() throws IOException {
        final String text = reader.read();
        final String replaceText = text.replaceAll(":c", "c:");
        printer.print(replaceText);
    }
}

