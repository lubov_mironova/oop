package ru.mla.replace.printers;

public class AdvConsolePrinter implements IPrinter {

    @Override
    public void print(String text) {
        System.out.println(text);
        System.out.println("length: " + text.length());
    }
}