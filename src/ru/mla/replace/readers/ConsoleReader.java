package ru.mla.replace.readers;

import java.util.Scanner;

public class ConsoleReader implements IReader {
    public static Scanner scanner = new Scanner(System.in);

    String string = scanner.nextLine();

    public ConsoleReader() {
    }

    @Override
    public String read() {
        return string;
    }

    public ConsoleReader(String string) {
        this.string = string;
    }
}