package ru.mla.replace.readers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class FileReader implements IReader {
    private String fileName;

    public FileReader(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public String read() {
        String listLine = "";
        try {
            List<String> list = Files.readAllLines(Paths.get(fileName));

            int size = list.size();
            for (int i = 0; i < size; i++) {
                listLine += list.get(i);
            }
        } catch (IOException e) {
            System.out.println("error 404 - not found");
        }

        return listLine;
    }
}