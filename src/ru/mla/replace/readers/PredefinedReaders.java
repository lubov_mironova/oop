package ru.mla.replace.readers;

public class PredefinedReaders implements IReader {
    private String text;

    public PredefinedReaders(String text) {
        this.text = text;
    }

    @Override
    public String read() {
        return text;
    }
}