package ru.mla.prefix;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Prefix {
    public static void main(String[] args) {
        String s;
        Pattern pattern = Pattern.compile("\\b(пре|при|Пре|При)[а-яё]+\\b");
        ArrayList<String> text = new ArrayList<>();
        Matcher matcher = pattern.matcher("");
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("src\\ru\\mla\\prefix\\input"))) {
            while ((s = bufferedReader.readLine()) != null) {
                matcher.reset(s);
                while (matcher.find()) {
                    text.add(matcher.group());
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        System.out.println(text.toString());
        print(text);
    }

    public static void print(ArrayList<String> text) {
        try (FileWriter writer = new FileWriter("src\\ru\\mla\\prefix\\output")) {
            writer.write(String.valueOf(text));
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}