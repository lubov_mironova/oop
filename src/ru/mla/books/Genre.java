package ru.mla.books;

public enum Genre {
    ADVENTURE("Приключение"),
    EDUCATIONAL("Учебная"),
    FAIRY("Сказка"),
    FANTASY("Фантастика"),
    PROSE("Проза"),
    SCIENCE_FICTION("Научная фантастика");

    private String genre;

    Genre(String genre) {
        this.genre = genre;
    }

    public String getCyrillicGenre() {
        return genre;
    }
}

